var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var Word = mongoose.model('Word');

var request = require('request');

var fs = require('fs');
var readline = require('readline');

var multer  = require('multer')
var upload_folder = 'uploads/';
var upload_multer = multer({ dest: upload_folder });

var xml = require('xml');

/* GET word's list of synonyms. */
router.get('/synonyms/:wordString', function(req, res, next) {
	Word.findOne({
		title: req.params.wordString,
	}, function (err, data) {
		if(err) return console.error(err);
		if(data) res.json(data.synonymous);
		else res.json({ error: "Word not found" })
	});
});

/* GET all words starting with a given letter. */
router.get('/all', function(req, res, next) {
	Word.find({}, function (err, data) {
		if(err) return console.error(err);
		if(data) res.json(data.map(function (w) {
			return w.title;
		}));
		else res.json({ error: "Error has ocurred" })
	});
});

/* GET count of all stored words entries. */
router.get('/count', function (req, res, next) {
	Word.count({}, function (err, count) {
		if(err) return console.error(err);
		if(count) res.json(count);
	});
});

/* GET word's list from external text */
router.get('/longlist', function(req, res, next) {
	request.get('https://raw.githubusercontent.com/atebits/Words/master/Words/en.txt', function (error, response, body) {
	    if (!error && response.statusCode == 200) {
			var wordArray = body.split('\n');
			var len = wordArray.length;

			var i = 1;                     //  set your counter to 1

			function myLoop () {           //  create a loop function
				setTimeout(function () {    //  call a 3s setTimeout when the loop is called
					saveWord(wordArray[i]);
					i++;                     //  increment the counter
					if (i < len) {            //  if the counter < 10, call the loop function
						myLoop();             //  ..  again which will trigger another 
					}                        //  ..  setTimeout()
			   }, 10);
			}

			myLoop();  
			res.json({ message: 'File parsed: ' + wordArray.length + ' words.' });			
	    }
	});
});

function saveWord(word) {
	// console.log(word);

	request.get('https://words.bighugelabs.com/api/2/f184a3561fc9845d8be46ba99211e714/'+ word +'/json', 
		function (err, res, apiWords) {
	    if (!err && res.statusCode == 200) {
			var retWords = [];

			console.log(apiWords);

			for (var key in apiWords) {
				if(apiWords[key].hasOwnProperty('syn')) {
					retWords = retWords.concat(apiWords[key].syn);
				}
				// console.log(retWords);
			}

	    	console.log(word + ": " + apiWords);

			var wordToSave = {
				title: word,
				synonyms: retWords,
				rhymes: []
			};

			console.log(wordToSave);

			var newWord = new Word({
				title: word,
				synonymous: [],
				rhymes: []
			});

			// newWord.save(function(data, err) {
			// 	if(err) console.log(err);
			// });
	    }
	});
}

/* GET all words starting with a given letter. */
router.get('/starting/:letter', function(req, res, next) {
	letter = req.params.letter;
	
	if (letter.length != 1) {
		res.json({ error: "Use with a single letter" });
	}

	Word.find({
		title: { $regex : new RegExp('^'+letter, "i") }
	}, function (err, data) {
		if(err) return console.error(err);
		if(data) res.json(data.map(function (w) {
			return w.title;
		}));
		else res.json({ error: "No words for that letter" })
	});		
});

// { $regex : new RegExp(req.params.wordString, "i") }

/* GET word's list of synonyms in a STATIC page */
router.get('/static/:wordString', function(req, res, next) {
	Word.findOne({
		title: req.params.wordString,
	}, function (err, data) {
		if(err) return console.error(err);
		if(data) res.render('static', { data: data.synonymous, title: req.params.wordString });
	});		
});

/* Generates the sitemap.xml and stores it in public folder. */
router.get('/generateXml', function(req, res, next) {

	var date = new Date();
	var formattedDate = date.toISOString().slice(0,10);

	console.log(formattedDate);

	Word.find({
	}, 'title' ,function (err, data) {
		if (err) return console.error(err);
		var words = [{ _attr: { xmlns: 'http://www.sitemaps.org/schemas/sitemap/0.9', locale: 'US'} }];
		
		// for (var i = 0; i < 10; i++) {
		for (var i = 0; i < data.length; i++) {
			if(data[i].title != null) {
				words.push({
					url: [
						{ loc: req.protocol + '://' + req.get('host') + '/synonyms/' + data[i].title.split(' ').join('+') },
						{ lastmod: formattedDate },
						{ changefreq: 'monthly' }
						// { priority: 0.8 }
					]
				});
			}
		}

		var xmlObj =[ { urlset: words } ];
		var rootXml = xml(xmlObj, { declaration: true });

		fs.writeFile("public/sitemap.xml", rootXml, function(err) {
			if(err) {
				return console.log(err);
			}
			console.log("The file was saved!");
		}); 

		res.header('Content-Type', 'text/xml');
		res.set('Content-Type', 'text/xml');
		res.send(rootXml);
	});
});

/* GET word's list of rhymes. */
router.get('/rhymes/:wordString', function(req, res, next) {
	Word.findOne({
		title: req.params.wordString,
	}, function (err, data) {
		if(err) return console.error(err);
		if(data) res.json(data.rhymes);
		res.json({ error: "Word not found" })
	});
});

/* GET a word. */
router.get('/word/:wordString', function(req, res, next) {
	var word = req.params.wordString.split('+').join(' ');
	Word.find({
		title: { $regex : new RegExp('^'+word+'$', "i") }
	}, function (err, data) {
		if(err) return console.error(err);
		if(data) {
			res.json(data);
			return;
		}
		res.json({ error: "Word not found" });
	});
});

/* Search for a word or words given a partial string. */
router.get('/search/:wordString', function(req, res, next) {
	Word.find({
		// title: req.params.wordString
		title: new RegExp(req.params.wordString, 'i')
	}, function (err, data) {
		if (err) return console.error(err);
		res.json(data);
	});
});

/* Search for a word or words given a partial string. */
router.put('/edit/:wordString', function(req, res, next) {
	Word.update({
		title: req.params.wordString
	}, {
		$set: { 
			synonymous: req.body.synonyms,
			rhymes: req.body.rhymes
		}
	},
	function (err, data) {
		if (err) return console.error(err);
		res.json(data);
	});
});

/* Deletes a word. */
router.get('/delete/:wordString', function(req, res, next) {
	Word.remove({
		title: req.params.wordString
	},
	function (err, data) {
		if (err) return console.error(err);
		res.json(data);
	});
});

/* Cretaes new word */
router.post('/new', function(req, res, next) {
	var word = new Word({
		title: req.body.title,
		synonymous: req.body.synonyms,
		rhymes: []
	});

	word.save(function(data, err) {
		if(err) console.log(err);
		res.json(word)
	});
});

/* UPLOAD STUFF */
var type = upload_multer.single('recfile');

/* Upload a file. */
router.post('/upload', type, function (req, res, next) {
	var tmp_path = req.file.path;
	var target_path = upload_folder + req.file.originalname;

	var src = fs.createReadStream(tmp_path);
	var dest = fs.createWriteStream(target_path);
	src.pipe(dest);
	src.on('end', function() { 
		var savedFile = fs.createReadStream(target_path);
		readLines(savedFile, processLine);
		res.render('upload'); 
	});
	src.on('error', function(err) { res.render('error'); });
});
	
router.get('/parsefile/:fileName', function (req, res, next) {
	var inputSrc = 'uploads/'+req.params.fileName;
	parseFile(inputSrc);
	res.json({ message: 'File parsed.' });
});

function parseFile(source) {
	var instream = fs.createReadStream(source)
	var outstream = new stream;
	var rl = readline.createInterface(instream, outstream);

	var linecount = 0;

	rl.on('line', function(line) {
		// console.log(line);
		var print = false;
		linecount++;
		if(linecount % 500 === 0) {
			print = true;
			console.log(linecount);
		}
		setTimeout(processLine(line, print), 2000 + linecount*2 );
		print = false;
	});

	rl.on('close', function() {
		return;
	});	
}

function processLine(data, print) {
	var wordsList = data.split(',');
	var wordTitle = wordsList[0];
	var synonyms = wordsList.slice(1);

	var new_word = new Word({ 
		title: wordTitle,
		synonymous: synonyms
	});

	new_word.save(function(err) {
		if (err && err.code !== 11000 ) {
			console.log("Error! " + err);
			throw err;
		}
		if ( err && err.code === 11000 ) {
			if(print) console.log('error', '"' + wordsList[0] + '" already exists');
		} else {
			if(print) console.log('"' + wordsList[0] + '" saved successfully');
		}
	});
	// for (var i = 0; i < synonyms.length; i++) {
	// 	var new_word = new Word({ 
	// 		title: synonyms[i],
	// 		synonymous: [wordTitle]
	// 	});

	// 	new_word.save(function(err) {
	// 		if (err && err.code !== 11000 ) {
	// 			console.log("Error! " + err);
	// 			throw err;
	// 		}
	// 		if ( err && err.code === 11000 ) {
	// 			// if(print) console.log('error', '"' + wordsList[0] + '" already exists');
	// 		} else {
	// 			// if(print) console.log('"' + wordsList[0] + '" saved successfully');
	// 		}
	// 	});
	// }
}

function readLines(input, func) {
	var remaining = '';

	input.on('data', function(data) {
		remaining += data;
		var index = remaining.indexOf('\n');
		while (index > -1) {
			// console.log('reading');
			var line = remaining.substring(0, index);
			remaining = remaining.substring(index + 1);
			func(line);
			index = remaining.indexOf('\n');
		}
	});

	input.on('end', function() {
		if (remaining.length > 0) {
			func(remaining);
		}
	});
}

module.exports = router;