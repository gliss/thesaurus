var express = require('express');
var router = express.Router();

var request = require('request');

var mongoose = require('mongoose');
var Word = mongoose.model('Word');

router.get('/', function(req, res) {
	res.render('front', { 
		title: 'Synonyms',
		description: 'Choose synonyms with the handy Brainstorm Pro free online thesaurus checkbox tool. Check out our desktop thesaurus, chrome and firefox plugins and mobile thesaurus app downloads.', 
		keywords: 'synonyms, brainstorm pro, the saurus, checkbox tool', 
		word: '', 
		synonyms: [],
		rhymes: []
	});
});

router.get('/quickraps', function(req, res) {
	res.render('quickrap', { 
		title: 'Quick Raps',
		description: 'Quick Raps - Brainstorm Pro. Search for synonyms and rhymes.', 
		keywords: 'quickraps', 
		word: '', 
		synonyms: [],
		rhymes: []
	});
});

router.get('/synonyms', function(req, res) {
	res.redirect('/');
});

router.get('/synonyms/:word', function(req, res, next) {

	var word = req.params.word.split('+').join(' ');
	var rhymes = [];
	var synonyms = [];

	Word.findOne({
		// title: word,
		title: { $regex : new RegExp('^'+word+'$', "i") }
	}, function (err, data) {
		if(err) return next(err);
		if(data) {
			synonyms = data.synonymous;
			synonyms = synonyms.map(function(w) {
				return { word: w, link: w.split(' ').join('+'), selected: false };
			});

			rhymes = data.rhymes.map(function(w) {
				return { word: w, link: w.split(' ').join('+'), selected: false };
			});

			var descriptionString = 'Choose synonyms for ' + word + ' with the handy Brainstorm Pro free online thesaurus checkbox tool. Check out our desktop thesaurus, chrome and firefox plugins and mobile thesaurus app downloads.';
			// descriptionString += synonyms.slice(0,10).map(function(w) {
			// 	return w.word;
			// });
			// descriptionString += '.';

			var keywords = synonyms.slice(0,20).map(function(w) {
				return w.word;
			});

			res.render('front', { 
				title: synonyms.length + ' Synonyms for ' + word,
				description: descriptionString, 
				keywords: keywords, 
				word: word, 
				synonyms: synonyms,
				rhymes: rhymes
			});

			// getRhymes(data.rhymes, word).then(function(rhymes) {});
		} else {
			console.log('not found');
			res.render('front', { 
				title: 'Synonyms for ' + word,
				description: 'The Saurus - Brainstorm Pro. Search synonyms and rhymes.', 
				keywords: 'synonyms, rhymes', 
				word: word, 
				synonyms: synonyms,
				rhymes: rhymes
			});
		}
	});

});

router.get('/app', function(req, res) {
	// if(typeof(req.query._escaped_fragment_) !== "undefined" 
		// && req.url.indexOf('synonyms/') > -1) {
		// var wordParam = req.url.split('/')[3];
		// Word.findOne({
		// 	title: wordParam,
		// }, function (err, data) {
		// 	if(err) return console.error(err);
		// 	if(data) res.render('static', { data: data.synonymous, title: wordParam });
		// });
	// } else
		res.render('index');
});

router.get('/admin', function (req, res) {
	res.render('partials/admin');
});

// router.get('/edit/:word', function (req, res) {
// 	Word.find({
// 		title: req.params.word,
// 	}, function (err, data) {
// 		if(err) return console.error(err);
// 		if(data) {
// 			res.render('partials/edit', word);
// 		} else res.json({ error: "Word not found" });
// 	});
// });

router.get('/partials/:name', function (req, res) {
	var name = req.params.name;
	res.render('partials/' + name);
});

router.get('*', function(req, res) {
	res.render('index');
});

getRhymes = function(currRhymes, word) {
	if (currRhymes.length > 0) {
		return new Promise(
			function(resolve, reject) {
				var rhymes = currRhymes
					.map(function(w) {
						return { word: w, link: w.split(' ').join('+') };
					});
				resolve(rhymes);
			});
	};
	return new Promise(
		function(resolve, reject) {

			request('http://rhymebrain.com/talk?function=getRhymes&word=' + word,
				function (error, response, body) {
				if (!error && response.statusCode == 200) {
					var response = JSON.parse(response.body);
					var rhymes = response
						.filter(function(w) {
							return w.score >= 280;
						})
						.map(function(w) {
							return { word: w.word, link: w.word.split(' ').join('+') };
						});

					Word.update({
						title: word
					}, { 
						$set: {
							rhymes: rhymes.map(function(w) { return w.word })
						}
					}, function (err, data) {
						console.log(data);
						resolve(rhymes);
					});
				}
			});
		});
}

module.exports = router;