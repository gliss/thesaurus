var mongourl = 'mongodb://localhost/thesaurus';

// if OPENSHIFT env variables are present, use the available connection info:
if(process.env.OPENSHIFT_MONGODB_DB_PASSWORD){
	mongourl = process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +
	process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
	process.env.OPENSHIFT_MONGODB_DB_HOST + ':' +
	process.env.OPENSHIFT_MONGODB_DB_PORT + '/' +
	process.env.OPENSHIFT_APP_NAME;
}

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Word = new Schema({
	title: { 
		type: String, 
		unique: true,
		dropDups: true
	},
	synonymous: Array,
	rhymes: Array
});

var User = new Schema({
	name: { 
		type: String, 
		unique: true,
		dropDups: true
	},
	password: String,
	email: { 
		type: String, 
		unique: true,
		dropDups: true
	},
	admin: { type: Boolean, default: false }
});

mongoose.model('Word', Word);
mongoose.model('User', User);

mongoose.connect(mongourl);