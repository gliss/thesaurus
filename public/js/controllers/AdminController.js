app.controller('AdminController', [
	'$scope',
	'$routeParams',
	'wordsService',
	'PageService',
	function($scope, $routeParams, wordsService, PageService) {

		$scope.searchData = { "word": ""};
		$scope.wordsCount = 0;
		PageService.setTitle('Admin dashboard');

		wordsService.getCount()
			.success(function(data) {
				$scope.wordsCount = data;
			})
			.error(function(err) {
				console.log(err);
			});

		$scope.search = function(searchData) {
			console.log(searchData);
			$scope.searchData = searchData;
			if(searchData.word) {
				wordsService.search(searchData.word)
					.success(function(data) {
						$scope.words = data;
						$scope.synonyms = [];
						for (var i = 0; i < $scope.words.length; i++) {
							for (var j = 0; j < $scope.words[i].synonymous.length; j++) {
							 	$scope.synonyms.push({
								 	"name": $scope.words[i].synonymous[j]
							 	});
							 }
						}
					})
					.error(function(err) {
						console.log(err);
					});
			}
		}

}]);