app.controller('MainController', [
	'$scope',
	'wordsService',
	'rhymesApiService',
	'quotesApiService',
	'$routeParams',
	'PageService',
	'clipboard',
	'$location',
	'FileSaver',
	'Blob',
	function($scope, wordsService, rhymesApiService, quotesApiService,
		$routeParams, PageService, clipboard, $location, FileSaver, Blob) {

		$scope.afterDelimiter = ',';
		$scope.beforeDelimiter = '';
		$scope.toBePasted = '';
		$scope.workspaceArea = '';
		$scope.searchData = { "word": ""};

		$scope.activeQuote= 0;

		$scope.synonymsCollapsed = false;
		$scope.rhymesCollapsed = false;

		$scope.synonyms = [];
		$scope.rhymes = [];
		$scope.quickRhymes = [];

		$scope.quotes = [];
		$scope.word = "";

		if($routeParams['word']) {
			console.log('Get params');
			// var wordString = $routeParams['word'].replace(/\+/g, ' ');
			// $scope.searchData = { word: wordString };
			// PageService.setTitle('Synonyms for ' + $routeParams['word']);
			// internalSearch($scope.searchData);
		}

		$scope.search = function(searchData) {
			console.log(searchData);
			var currlocation = $location.protocol() + '://' + 
				$location.host() + 
				':' + $location.port() +
				 '/synonyms/' + searchData.word.split(' ').join('+');
			$scope.searchData = searchData;
			if(searchData.word) {
				window.location = currlocation;
				// internalSearch(searchData)
			}
		}

		$scope.brainstormWord = function(word) {
			window.location.pathname = "synonyms/" + word;
		}

		if(window.location.pathname.includes('/synonyms/')) {
			var word = window.location.pathname.split('/synonyms/').pop();
			internalSearch({word: word});
			$scope.word = word;
		}

		getQuotes();

		function internalSearch(searchData) {
			wordsService.getWord(searchData.word)
				.success(function(data) {
					$scope.words = data;
					$scope.synonyms = [];
					$scope.rhymes = [];
					for (var i = 0; i < $scope.words.length; i++) {
						getSynonyms($scope.words[i]);
						getRhymes($scope.words[i]);
					}

					if($scope.synonyms.length == 0) {
						console.log('no words');
						getFromHugeLabs(searchData.word);
					}

					// wordsService.getLongList()
					// 	.success(function(longList) {
					// 		console.log(longList.length);
							// console.log(Object.keys(longList).length);
							// $.each(longList, function(k, v) {
							    //display the key and value pair
							    // console.log(k);
							// });
						// })
						// .error(function(err) {
						// 	console.log(err);
						// });

					$scope.uncheckAllSynonyms();
				})
				.error(function(err) {
					console.log(err);
				});
		}

		function getFromHugeLabs(word) {
			wordsService.getFromHugeLabs(word)
				.success(function(apiWords) {
					var retWords = [];

					for (var key in apiWords) {
						if(apiWords[key].hasOwnProperty('syn')) {
							retWords = retWords.concat(apiWords[key].syn);
						}
					}

					var wordToSave = {
						title: word,
						synonyms: retWords
					};

					wordsService.newWord(wordToSave)
						.success(function(data) {
							console.log(data);
						})
						.error(function(err) {
							console.log(err);
						});

					$scope.synonyms = retWords.map(function (w) {
						return buildWord(w);
					});;

				})
				.error(function(err) {
					console.log(err);
				});			
		}

		function getQuotes() {
			console.log('getting quotes')
			// quotesApiService.get()
			// 	.success(function(data) {
			// 		var returnData = data;
			// 		console.log('quotes')
			// 		console.log(returnData);
			// 	})
			// 	.error(function(err) {
			// 		console.log(err);
			// 	});
			$scope.quotes = [
				{
					"name": "Julia Cameron",
					"content": "Mystery is at the heart of creativity. That, and surprise."
				},
				{
					"name": "Henri Matisse",
					"content": "Creativity takes courage."
				},
				{
					"name": "Martin Villeneuve",
					"content": "Problems are hidden opportunities, and constraints can actually boost creativity."
				},
				{
					"name": "Julia Cameron",
					"content": "Creativity - like human life itself - begins in darkness."
				},
				// {
				// 	"name": "Lapo Elkann",
				// 	"content": "I'm a freestyle creative entrepreneur. Not a businessman. I like to create ventures in which creativity stands at the centre."
				// },
				// {
				// 	"name": "Otto Rank",
				// 	"content": "The new meaning of soul is creativity and mysticism. These will become the foundation of the new psychological type and with him or her will come the new civilization."
				// }
			];
		}

		$scope.setVisible = function(index) {
			// console.log("index:" + index);
			if (index == $scope.activeQuote) {           
				return("1") ;
			} else {
				return("0") ;
			}
		}

		$scope.refresh = function() {
		
			// console.log("refresh") ;
			// console.log("Active Message = " + $scope.activeQuote) ;
			// console.log("Max messages = " +  $scope.quotes.length) ;

			$scope.$apply(function() {
					$scope.activeQuote++ ;
					if ($scope.activeQuote >= $scope.quotes.length) {
						$scope.activeQuote = 0 ;
					}
				}
			)
		}

		setInterval($scope.refresh, 5000) ; 

		function getSynonyms(word) {
			$scope.synonyms = word.synonymous.map(function (w) {
				return buildWord(w);
			});

			$scope.synonyms.push(buildWord(word.title));
		}

		function getRhymes(word) {
			if(word.rhymes.length > 0) {
				$scope.rhymes = word.rhymes.map(function (w) {
					return buildWord(w);
				});
			} else {
				rhymesApiService.search(word.title)
					.success(function(data) {
						var returnData = data.filter(function(val) {
							return val.score >= 280;
						});

						var rhymesToSave = [];
						console.log(returnData);
						for (var j = 0; j < returnData.length; j++) {
							$scope.rhymes.push({ 
								"name": returnData[j].word,
								"link": returnData[j].word.split(' ').join('+')
							});
							rhymesToSave.push(returnData[j].word);
						}

						if(rhymesToSave.length > 0) {
							var wordToSave = {
								title: word.title,
								synonyms: word.synonymous,
								rhymes: rhymesToSave
							};

							wordsService.updateWord(wordToSave)
								.success(function(data) {
									console.log(data);
								})
								.error(function(err) {
									console.log(err);
								});
						}

					})
					.error(function(err) {
						console.log(err);
					});
			}
		}

		buildWord = function (word) {
			return { "name": word, "link": word.split(' ').join('+') };
		}

		$scope.checkAllSynonyms = function () {
			$scope.selectedAll = true;
			angular.forEach($scope.synonyms, function (item) {
				item.Selected = $scope.selectedAll;
			});
		};
		$scope.uncheckAllSynonyms = function () {
			$scope.selectedAll = false;
			angular.forEach($scope.synonyms, function (item) {
				item.Selected = $scope.selectedAll;
			});
		};

		$scope.checkAllRhymes = function () {
			$scope.rhymesSelectedAll = true;
			angular.forEach($scope.rhymes, function (item) {
				item.Selected = $scope.rhymesSelectedAll;
			});
		};
		$scope.uncheckAllRhymes = function () {
			$scope.rhymesSelectedAll = false;
			angular.forEach($scope.rhymes, function (item) {
				item.Selected = $scope.rhymesSelectedAll;
			});
		};

		$scope.synonymsReverse = false;
		$scope.synonymsOrder = 'name';
		$scope.rhymesReverse = false;
		$scope.rhymesOrder = 'name';

		$scope.orderAZSynonyms = function () {
			$scope.synonymsOrder = 'name';
			$scope.synonymsReverse = false;
		};

		$scope.orderZASynonyms = function () {
			$scope.synonymsOrder = 'name';
			$scope.synonymsReverse = true;
		};

		$scope.orderLengthHigherToLowerSynonyms = function() {
			$scope.synonymsOrder = ['name.length','name'];
			$scope.synonymsReverse = true;
		}

		$scope.orderLengthLowerToHigherSynonyms = function() {
			$scope.synonymsOrder = ['name.length','name'];
			$scope.synonymsReverse = false;
		}

		$scope.orderAZRhymes = function () {
			$scope.rhymesOrder = 'name';
			$scope.rhymesReverse = false;
		};

		$scope.orderZARhymes = function () {
			$scope.rhymesOrder = 'name';
			$scope.rhymesReverse = true;
		};

		$scope.orderLengthHigherToLowerRhymes = function() {
			$scope.rhymesOrder = ['name.length','name'];
			$scope.rhymesReverse = true;
		}

		$scope.orderLengthLowerToHigherRhymes = function() {
			$scope.rhymesOrder = ['name.length','name'];
			$scope.rhymesReverse = false;
		}

		$scope.copySelected = function() {
			var filtered = [];

			[$scope.synonyms, $scope.rhymes].forEach(function (array) {
				filtered = filtered.concat(filterAndParse(array));
			});

			if(filtered.length == 0) {
				addAlert('No words to copy.', 'danger');
				return;
			}

			$scope.toBePasted = filtered.join('');
			addAlert(filtered.length + ' words copied to clipboard.', 'success');
			clipboard.copyText($scope.toBePasted);
		}

		filterAndParse = function (list) {
			return list.filter(function (s) {
				return s.Selected;
			})
			.map(function (s) {
				return s.name;
			})
			.map(function (s) {
				return $scope.beforeDelimiter + s + $scope.afterDelimiter;
			});
		}

		filter = function (list) {
			return list.filter(function (s) {
				return s.Selected;
			})
			.map(function (s) {
				return s.name;
			});
		}

		$scope.pasteToWorkspace = function() {
			$scope.workspaceArea = $scope.toBePasted;
		}
		
		$scope.clearClipboard = function() {
			$scope.toBePasted = '';
			clipboard.copyText('');
			addAlert('Clipboard clear.', 'info');
		}

		addAlert = function (text, type) {
			var icons = {
				'success': 'ok-sign',
				'info': 'info-sign',
				'danger': 'exclamation-sign'
			};
			$scope.alert = { type: type, text: text , icon: icons[type]};
		}

		$scope.saveDownload = function() {
			// console.log(text);
			var text = $scope.toBePasted;
			if(text.length > 0) {
				var data = new Blob([text], { type: 'text/plain;charset=utf-8' });
				FileSaver.saveAs(data, 'Brainstorm Pro synonyms list for ' + $scope.word + '.txt');
			}
		};

		$scope.quickRapsSearch = function() {
			var wordsList = $scope.search.words.split(',');
			wordsList = wordsList.map(function (word) {
				return word.trim();
			});

			wordsList = wordsList.slice(0,5);

			$scope.synonyms = [];
			wordsList.forEach(function(word, index) {
				if(word != '') {
					wordsService.getWord(word)
						.success(function(data) {
							$scope.words = data;
							for (var i = 0; i < $scope.words.length; i++) {
								var currSynonyms = $scope.words[i].synonymous.map(function (w) {
									return buildWord(w);
								});
								console.log(currSynonyms);
								$scope.synonyms = $scope.synonyms.concat(currSynonyms);
							}

							$scope.uncheckAllSynonyms();
						})
						.error(function(err) {
							console.log(err);
						});
				}
			});
		}

		$scope.findRhymes = function() {
			var filtered = [];

			filtered = filter($scope.synonyms);

			if(filtered.length == 0) {
				addAlert('No words to copy.', 'danger');
				return;
			}

			console.log(filtered);

			filtered.forEach(function (word) {
				rhymesApiService.search(word)
					.success(function(data) {
						var returnData = data.filter(function(val) {
							return val.score >= 280;
						}).map(function (word) {
							return word.word;
						});

						console.log(word);
						console.log(returnData);

						var message = "";

						if(returnData.length == 0) {
							message = "No Perfect Rhymes Found";
						}

						$scope.quickRhymes.push({ 
							"word": word,
							"rhymes": returnData,
							"message": message
						});
					})
					.error(function(err) {
						console.log(err);
					});				
			})
		}		
}]);