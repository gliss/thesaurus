app.controller('EditController', [
	'$scope',
	'$routeParams',
	'wordsService',
	'PageService',
	function($scope, $routeParams, wordsService, PageService) {

		PageService.setTitle('Edit word');

		$scope.wordToEdit = {};

		if($routeParams['word']) {
			var wordString = $routeParams['word'];
			wordsService.getWord(wordString)
				.success(function(data) {
					$scope.wordToEdit = {
						title: data[0].title,
						synonyms:  data[0].synonymous.join(','),
						rhymes: data[0].rhymes.join(',')
					};
				})
				.error(function(err) {
					console.log(err);
				});		
		}

		$scope.saveWord = function(wordToEdit) {
			var currSynonyms = wordToEdit.synonyms != '' ? wordToEdit.synonyms.split(',') : []; 
			var currRhymes = wordToEdit.rhymes != '' ? wordToEdit.rhymes.split(',') : [];

			var wordToSave = {
				title: wordToEdit.title,
				synonyms: currSynonyms,
				rhymes: currRhymes
			};

			wordsService.updateWord(wordToSave)
				.success(function(data) {
					console.log(data);
					$scope.alert = { type: 'success', text: 'Word saved successfully!', icon: 'ok-sign'};					
				})
				.error(function(err) {
					$scope.alert = { type: 'danger', text: 'Word not saved, contact admin.', icon: 'exclamation-sign'};					
					console.log(err);
				});				
		}
}]);