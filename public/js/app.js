var app = angular.module('theSaurusApp', [
	'ngRoute',
	'ngAnimate',
	'ui.bootstrap',
	'ngStorage',
	'angular-clipboard',
	'ngFileSaver'
]);

// app.config(['$routeProvider', 
// 	'$locationProvider', 
// 	'$httpProvider', 
// 	function ($routeProvider, $locationProvider, $httpProvider) {
// 	$routeProvider.
// 		when('/', {
// 			templateUrl: 'partials/front',
// 			controller: 'MainController'
// 		}).
// 		when('/synonyms/:word', {
// 			templateUrl: 'partials/front',
// 			controller: 'MainController'
// 		}).
// 		when('/admin', {
// 			templateUrl: 'partials/admin',
// 			controller: 'AdminController'
// 		}).
// 		when('/edit/:word', {
// 			templateUrl: 'partials/edit',
// 			controller: 'EditController'
// 		}).
// 		when('/upload', {
// 			templateUrl: 'partials/upload',
// 			controller: 'UploadController'
// 		}).
// 		otherwise({
// 			redirectTo: '/'
// 		});
	
// 		$locationProvider.html5Mode(true);
// }]);

	
	// $httpProvider.interceptors.push(['$q', 
	// 	'$location', 
	// 	'$localStorage', 
	// 	function ($q, $location, $localStorage) {
	// 	return {
	// 		'request': function (config) {
	// 			config.headers = config.headers || {};
	// 			if ($localStorage.token) {
	// 				config.headers['x-access-token'] = $localStorage.token;
	// 			}
	// 			return config;
	// 		},
	// 		'responseError': function (response) {
	// 			if (response.status === 401 || response.status === 403) {
	// 				$location.path('/login');
	// 			}
	// 			return $q.reject(response);
	// 		}
	// 	};
	// }]);