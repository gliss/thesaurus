app.factory('PageService', function(){
	var title = 'Synonyms';
	return {
		title: function() { return title; },
		setTitle: function(newTitle) { title = newTitle; }
	};
});