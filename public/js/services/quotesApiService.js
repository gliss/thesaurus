app.factory('quotesApiService', ['$http', function($http) {

	this.getQuotes = function() {
		return $http.get('http://www.brainstormpro.com/inc1/q1.json')
			.success(function(data) {
				return data;
			})
			.error(function(err) {
				console.log(err);
				return err;
			});
	};

	return {
		get: this.getQuotes
	}

}]);