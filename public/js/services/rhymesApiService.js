app.factory('rhymesApiService', ['$http', function($http) {

	var SCORE_LIMIT = 280;

	this.searchWords = function(wordString) {
		return $http.get('http://rhymebrain.com/talk?function=getRhymes&word=' + wordString)
			.success(function(data) {
				return data;
			})
			.error(function(err) {
				console.log(err);
				return err;
			});
	};

	return {
		search: this.searchWords
	}

}]);