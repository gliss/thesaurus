app.factory('wordsService', ['$http', function($http) {

	this.searchWords = function(wordString) {
		return $http.get('/api/search/' + wordString)
			.success(function(data) {
				console.log(data);
				return data;
			})
			.error(function(err) {
				console.log(err);
				return err;
			});
	};

	this.getWord = function(wordString) {
		return $http.get('/api/word/' + wordString)
			.success(function(data) {
				console.log(data);
				return data;
			})
			.error(function(err) {
				console.log(err);
				return err;
			});
	};

	this.getCount = function() {
		return $http.get('/api/count/')
			.success(function(data) {
				return data;
			})
			.error(function(err) {
				return err;
			});
	};

	this.updateWord = function(wordObject) {
		return $http.put('/api/edit/' + wordObject.title, wordObject)
			.success(function(data) {
				return data;
			})
			.error(function(err) {
				console.log(err);
				return err;
			});
	};

	this.newWord = function(wordObject) {
		return $http.post('/api/new', wordObject)
			.success(function(data) {
				return data;
			})
			.error(function(err) {
				console.log(err);
				return err;
			});
	};

	this.getFromHugeLabs = function(wordString) {
		return $http.get('https://words.bighugelabs.com/api/2/f184a3561fc9845d8be46ba99211e714/'+ wordString +'/json')
			.success(function(data) {
				return data;
			})
			.error(function(err) {
				console.log(err);
				return err;
			});
	}

	this.getLongList = function() {
		// return $http.get('https://raw.githubusercontent.com/adambom/dictionary/master/dictionary.json')
		// return $http.get('https://raw.githubusercontent.com/jacksonrayhamilton/wordlist-english/master/english-words.json')
		return $http.get('https://raw.githubusercontent.com/atebits/Words/master/Words/en.txt')
			.success(function(data) {
				var wordArray = fs.readFileSync(wordListPath, 'utf8').split('\n');
				return wordArray;
			})
			.error(function(err) {
				console.log(err);
				return err;
			});
	}

	return {
		search: this.searchWords,
		newWord: this.newWord,
		getWord: this.getWord,
		getCount: this.getCount,
		updateWord: this.updateWord,
		getFromHugeLabs: this.getFromHugeLabs,
		getLongList: this.getLongList
	}

}]);